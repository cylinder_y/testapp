package com.epam.testapp.database.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.testapp.exception.system.dao.DaoException;
import com.epam.testapp.model.News;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:WebContent/WEB-INF/test/spring-config.xml", })
public class NewsDaoTest {
	private long idOfSavedNews;

	@Autowired
	private INewsDao newsDao;

	@Autowired
	@Qualifier("news")
	private News news;

	@Autowired
	@Qualifier("emptyNews")
	private News emptyNews;

	@Test
	public void test() {
		assertNotNull(newsDao);
	}

	@Test
	public void testNewsDaoGetList() throws Exception {
		List<News> list = null;
		list = newsDao.getList();
		assertNotNull("List is null", list);
	}

	@Test(expected = DaoException.class)
	public void testNewsDaoSave() throws Exception {
		long savedId = newsDao.save(emptyNews);
		assertNotNull("Object is not saved", savedId);
	}

	@Test
	public void testNewsDaoSaveGood() throws Exception {
		idOfSavedNews = newsDao.save(news);
		news.setId(idOfSavedNews);
		assertNotNull("Object is not saved", idOfSavedNews);
	}

	@Test
	public void fetch() throws Exception {
		News news = newsDao.fetchById(this.news.getId());
		assertEquals(news, this.news);
	}

	@Test
	public void testNewsDaoDelete() throws Exception {
		List<News> list = newsDao.getList();
		int before = list.size();
		boolean result = newsDao.remove(new long[] { news.getId() });
		list = newsDao.getList();
		int after = list.size();
		assertEquals(before - 1, after);
		assertEquals(result, true);
	}

}
