<%@ page contentType="text/css;charset=UTF-8"%>
a { 
    color: blue;
}

.clearfix:after {
    content: " "; /* Older browser do not support empty content */
    visibility: hidden;
    display: block;
    height: 0;
    clear: both;
}

.error {
	color: red;
}

.site-wrapper{
    border: 1px solid black;
    width: 960px;
    min-height: 100%;
    margin: 0 auto;
}

.header {
    padding: 15px;
}


.header h1 {
    color: blue;
    font: 24px bolid;
    margin: 10px;
    float: left;
}

.header .language-switcher {
    margin: 34px 0 0 0;
    float: right;
}

.header .language-switcher a {
    margin: 0 10px 0 0;
}

.news-controls button {
    width: 100px;
}

.content-wrapper {
    padding: 10px 5px 5px 10px;
    border-top: 1px solid black;
    border-bottom: 1px solid black;
}

.content-wrapper .sidebar {
    padding: 10px 0 20px 10px;
    background-color: #a9a9a9;
    box-sizing: border-box;
    width: 210px;
    float: left;
}

.content-wrapper .sidebar p {
    padding: 5px 0 10px 0;
    color: white;
    text-align: center;
    background-color: #696969;
}

.content-wrapper .sidebar .navigation {
    margin: 5px 5px 0 20px;
    background-color: white;
    list-style-image: url('${pageContext.request.contextPath}/png/orange_button.png');
}

.content-wrapper .sidebar .navigation {
    padding: 10px 0 20px 25px;
}

.content-wrapper .sidebar .navigation li {
    margin: 0 0 10px 0;
}

.content-wrapper .sidebar .navigation a:hover {
    color: orange;
}

.content-wrapper .content {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    padding: 10px 25px 60px 20px;
    border: 1px solid #a9a9a9;
    float: right;
    width: 720px;
    min-height: 700px;
}

.content  .main-page-link {
    display: inline;
}

.main-page-link + p {
    display: inline-block;
}

.content  .main-page-link p{
    display: inline-block;
    margin: 0 5px 0 5px;
}

.content .main-page-link p, .content .main-page-link a {
    color: #a9a9a9;
}

.footer {
    padding: 2px;
}

.footer p {
    text-align: center;
}

/*/news-view-from-------------------------------------------*/
.news-view-from .news {
    margin: 30px 0 30px 30px
}


.news-view-from th{
    width: 100px;
    text-align: left;
    vertical-align: top;
    color: #a9a9a9;
}

.news-view-from td{
    display: inline-block;
    max-width: 450px;
    text-align: justify;
    margin: 0 0 20px 0;
}

.news-view-from .news-controls {
    padding: 50px 20px 0 0;
    float: right;
}

.formatted-text {
	 word-wrap: break-word;
	 white-space: pre-wrap;
}

/*/news-list-from-------------------------------------------*/

.news-list-from .news {
    margin: 30px 0 30px 30px
}

.news-list-from .news-controls a{
    margin: 0 10px 0 0;
    text-decoration: none;
}

.news-list-from p{
  	text-overflow: ellipsis;
}

.news-list-from .news-list-number {
    display: none;
}

.news-list-from .news-title {
    padding: 0 5px 0 0;
    display: inline;   
    font-weight : bold;
    max-width: 100%;
}

.news-list-from .title-and-brief {
	margin: 0 0 20px 0;
	display:inline-block;	
    max-width: 80%;
}

.news-list-from .news-brief {    
    display: inline;
    font-weight : bold;
    max-width: 100%;
}

.news-list-from .news-date {
    float: right;
    padding: 0 20px 0 0;
    text-decoration: underline;
}

.news-list-from .news-content {
    margin: 0 0 20px 0;
    text-align: justify;
    max-width: 65%;
}

.news-list-from .news-controls {
	/* padding: 0 0 8px 0; */
    float:right;
}

/* .news-list-from .news-controls button {
    width: 100px;
} */

/*news-edit-form
*/

.news-edit-from .news {
    margin: 30px 0 30px 30px
}


.news-edit-from th{
    width: 100px;
    text-align: left;
    vertical-align: top;
}

.news-edit-from td{
    width: 500px;
    display: inline-block;
    max-width: 100%;
    margin: 0 0 20px 0;
}





.news-edit-from .title-input  {
    width: 90%;
}

.news-edit-from .brief-input {    
    width: 100%;
    height: 100px;
}

.news-edit-from .content-input {
    width: 100%;
    height: 200px;
}

.news-edit-from textarea{
    resize: none;
    overflow: scroll;
}


.news-edit-from .news-controls {
    padding: 30px 20px 0 0;
    text-align: center;
}

