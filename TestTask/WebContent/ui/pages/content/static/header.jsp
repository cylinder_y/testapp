<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<div class="header clearfix">
	<h1>
		<bean:message key="header.title" />
	</h1>
	<div class="language-switcher">
		<html:link action="/Action">
			<html:param name="method" value="switchLanguage" />
			<html:param name="locale" value="en_US" />
			<bean:message key="header.link.english" />
		</html:link>
		<html:link action="/Action">
			<html:param name="method" value="switchLanguage" />
			<html:param name="locale" value="ru_RU" />
			<bean:message key="header.link.russian" />
		</html:link>
	</div>
</div>