<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<div class="sidebar">
	<p>
		<bean:message key="menu.title" />
	</p>
	<ul class="navigation">
		<li><html:link action="/Action">
				<html:param name="method" value="listView" />
				<bean:message key="menu.link.newsList" />
			</html:link></li>
		<li><html:link action="/Action">
				<html:param name="method" value="addNews" />
				<bean:message key="menu.link.addNews" />
			</html:link></li>
	</ul>
</div>