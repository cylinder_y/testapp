<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script async type="text/javascript"
	src="${pageContext.request.contextPath}/js/script.jsp"></script>
<html:form styleClass='news-edit-from' action="Action.do" method="post"
	onsubmit="return validate()">

	<div class="main-page-link">
		<html:link action="/Action.do?">
			<html:param name="method" value="listView" />
			<bean:message key="menu.title" />
		</html:link>
		<p>&gt;&gt;</p>
	</div>
	<p>
		<bean:message key="addNews.pageTitle" />
	</p>
	<div class="news clearfix">
		<table>
			<tr>
				<th><bean:message key="news.title" /></th>
				<td><html:text styleId="title" name="newsForm"
						property="news.title" styleClass="title-input"></html:text></td>
			</tr>
			<tr>
				<th><bean:message key="news.date" /></th>
				<td><html:hidden styleId="date" name="newsForm" property="news.date" ></html:hidden>
					<c:set var="pattern">
						<bean:message key="date.pattern" />
					</c:set>
					<input id="fakeDate" type="text" value="<bean:write name='newsForm' property='news.date' format='${pattern}' />">						
				</td>
			</tr>
			<tr>
				<th><bean:message key="news.brief" /></th>
				<td><html:textarea styleId="brief" name="newsForm"
						property="news.brief" styleClass="brief-input"></html:textarea></td>
			</tr>
			<tr>
				<th><bean:message key="news.content" /></th>
				<td><html:textarea styleId="content" name="newsForm"
						property="news.content" styleClass="content-input"></html:textarea></td>
			</tr>
		</table>
		<div class="news-controls">
			<button type="submit" name="method" value="save"
				onclick='getValue(this.value);'>
				<bean:message key="addNews.button.save" />
			</button>
			<%-- <html:submit title="method"><bean:message key="addNews.button.save"/></html:submit> --%>
			<%-- <html:button property="save"><bean:message key="addNews.button.save"/></html:button> --%>
			<button type="submit" name="method" value="cancel" onclick='getValue(this.value);'>
				<bean:message key="addNews.button.cancel" />
			</button>
			<%-- <html:button property="cancel"><bean:message key="addNews.button.cancel"/></html:button> --%>
		</div>
		<html:errors />
	</div>
</html:form>
