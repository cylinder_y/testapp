<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html"%>
<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean"%>
<%@ taglib prefix="logic" uri="http://struts.apache.org/tags-logic"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script async type="text/javascript"
	src="${pageContext.request.contextPath}/js/script.jsp">
</script>
<html:form styleClass='news-list-from' action="Action.do" method="post"
	onsubmit="return deleteNewsList()">
	<div class="main-page-link">
		<html:link action="/Action.do">
			<html:param name="method" value="listView" />
			<bean:message key="menu.title" />
		</html:link>
		<p>&gt;&gt;</p>
	</div>
	<p>
		<bean:message key="listView.pageTitle" />
	</p>
	<logic:notEmpty name="newsForm" property="newsList">
		<logic:iterate name="newsForm" id="news" property="newsList">
			<div class="news clearfix">
				<p class="news-list-number">
					<bean:write name="news" property="id" />
				</p>
				<div class="title-and-brief formatted-text"><p class="news-title"><bean:write name="news" property="title" /></p><p class="news-brief"><bean:write name="news" property="brief" /></p>
				</div>
				<c:set var="pattern">
						<bean:message key="date.pattern" />
				</c:set>
				<p class="news-date clearfix">
					<bean:write name="news" property="date" format="${pattern}" />
				</p>
				<p class="news-content formatted-text"><bean:write name="news" property="content" /></p>
				<div class="news-controls">
					<html:link action="/Action.do?method=newsView" paramId="ids"
						paramName="news" paramProperty="id" property="${news.id}">
						<bean:message key="listView.link.view" />
					</html:link>
					<html:link action="/Action.do?method=edit" paramId="ids"
						paramName="news" paramProperty="id" property="${news.id}">
						<bean:message key="listView.link.edit" />
					</html:link>
					<html:multibox styleId="ids" property="ids" name="newsForm"
						value="${news.id}"></html:multibox>
				</div>
			</div>
		</logic:iterate>
		<div class="news-controls">
			<button type="submit" name="method" value="delete"
				onclick="getValue(this.value);">
				<bean:message key="listView.button.delete" />
			</button>
		</div>
	</logic:notEmpty>

	<logic:empty name="newsForm" property="newsList">
		<div class="news">
			<bean:message key="listView.empty" />
		</div>
	</logic:empty>

</html:form>