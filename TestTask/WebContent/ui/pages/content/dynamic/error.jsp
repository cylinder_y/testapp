<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="exception" value="${pageContext.exception}"></c:set>
<c:choose>
	<c:when test='${pageContext.errorData.statusCode == 404}'>
		<bean:message key="error.page.404" />
	</c:when>
	<c:otherwise>
		<bean:message key="error.page" />
	</c:otherwise>
</c:choose>
