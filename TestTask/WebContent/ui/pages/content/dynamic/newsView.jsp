<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html"%>
<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean"%>
<%@ taglib prefix="logic" uri="http://struts.apache.org/tags-logic"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script async type="text/javascript"
	src="${pageContext.request.contextPath}/js/script.jsp">
</script>
<html:form styleClass='news-view-from' action="/Action.do" method="get">
	<div class="main-page-link">
		<html:link action="/Action">
			<html:param name="method" value="listView" />
			<bean:message key="menu.title" />
		</html:link>
		<p>&gt;&gt;</p>
	</div>
	<p>
		<bean:message key="newsView.pageTitle" />
	</p>
	<div class="news clearfix">
		<logic:notEmpty name="newsForm" property="news">
			<table>
				<tr>
					<th><bean:message key="news.title" /></th>
					<td class="formatted-text"><bean:write name="newsForm" property="news.title" /></td>
				</tr>
				<tr>
					<th><bean:message key="news.date" /></th>
					<td ><c:set var="pattern">
							<bean:message key="date.pattern" />
						</c:set>
						 <bean:write name="newsForm" property="news.date"	format="${pattern}" />
					</td>
				</tr>
				<tr>
					<th><bean:message key="news.brief" /></th>
					<td class="formatted-text"><bean:write name="newsForm" property="news.brief" /></td>
				</tr>
				<tr>
					<th><bean:message key="news.content" /></th>
					<td class="formatted-text"><bean:write name="newsForm" property="news.content" /></td>
				</tr>
			</table>
			<div class="news-controls">
				<button type="submit" name="method" value="edit">
					<bean:message key="newsView.button.edit" />
				</button>
				<button type="submit" name="method" value="delete"
					onclick="return confirmDelete(true);">
					<bean:message key="newsView.button.delete" />
				</button>				
				<%-- 	<html:button property="edit"><bean:message key="newsView.button.edit"/></html:button>
	        	<html:button property="delete"><bean:message key="newsView.button.delete"/></html:button> --%>
			</div>
		</logic:notEmpty>

	</div>
</html:form>