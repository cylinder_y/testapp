<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/ui/css/reset.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/ui/css/style.jsp" />
<tiles:importAttribute name="title" scope="page" />
<title><bean:message key="${title}" /></title>
</head>
<body>
	<c:choose>
		<c:when test='${param.method == "delete"}'>
			<c:set var="previosPage"
				value="${pageContext.request.contextPath}/Action.do?method=listView"
				scope="session" />
		</c:when>
		<c:when test='${param.method == "save"}'>
			<c:set var="previosPage"
				value="${pageContext.request.contextPath}/Action.do?method=newsView"
				scope="session" />
		</c:when>
		<c:when test='${param.method == null}'>
			<c:set var="previosPage"
				value="${pageContext.request.contextPath}/Action.do?method=listView"
				scope="session" />
		</c:when>
		<c:when test='${fn:contains(header.referer, "method")}'>
			<c:if
				test='${!fn:contains(header.referer, "addNews") && !fn:contains(header.referer, "edit")}'>
				<c:set var="previosPage" value="${header.referer}" scope="session" />
			</c:if>
		</c:when>
	</c:choose>
	<div class="site-wrapper">
		<tiles:insert attribute="header" />
		<div class="content-wrapper clearfix">
			<tiles:insert attribute="menu" />
			<div class="content">
				<tiles:insert attribute="content" />
			</div>
		</div>
		<tiles:insert attribute="footer" />
	</div>
</body>
</html>


