<%@ page contentType="text/javascript;charset=UTF-8"%>
<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
var button;
var save = "save";
var del = "delete";
var locale = '${sessionScope["org.apache.struts.action.LOCALE"]}';
var dateInput = "<bean:write name="newsForm" property="news.date" />";
var message = '';

function deleteNewsList() {
	if (button == del) {
		var chk = document.getElementsByName('ids');
		var ask = false;
		
		if (chk != null && chk.length >= 1) {
			for (var i = 0; i < chk.length; i++) {
				if (chk.item(i).checked) {
					ask = true;
					break;
				}
			}
		}
		return confirmDelete(ask, chk);		
	}
	return true;
}

function confirmDelete(ask, chk) {
	var confirmMessage;
	if (chk == null) {
		confirmMessage = "<bean:message key='listView.delete.confirmMessage'/>";
	} else if (isOneBoxCkecked(chk)) {
		confirmMessage = "<bean:message key='listView.delete.oneNews.confirmMessage'/>";		
	} else {
		confirmMessage = "<bean:message key='listView.delete.list.confirmMessage'/>";		
	}
	
	if (ask) {
		return confirm(confirmMessage);
	} else {
		alert("<bean:message key='listView.errorMessage'/>");
		return false;
	}
}

function isOneBoxCkecked(chk) {
	var ckeckedCount = 0;
	for (var i = 0; i < chk.length; i++) {
		if (chk.item(i).checked) {
			ckeckedCount++;
			if (ckeckedCount > 1) {
				return false;
			}
		}
	}
	
	if (ckeckedCount != 0) {
		return true;
	}
	
	return false;
}

function getValue(value) {
	button = value;	
}

function validate() {
	if (button == save) {
		var val = true;
		var title = document.getElementById('title').value;
		var brief = document.getElementById('brief').value;
		var content = document.getElementById('content').value;
		
		
		if (title == "") {
			val = false;
			message += "<bean:message key='error.news.title.required'/>" + '\n';
		} else if (title.length > 100) {
			val = false;
			message += "<bean:message key='error.news.title.length'/>" + '\n';
		}
		
		if (brief == "") {
			val = false;
			message += "<bean:message key='error.news.brief.required'/>" + '\n';
		} else if (brief.length > 500) {
			val = false;
			message += "<bean:message key='error.news.brief.length'/>" + '\n';
		}
		
		if (content == "") {
			val = false;
			message += "<bean:message key='error.news.content.required'/>" + '\n';
		} else if (content.length > 2048) {
			val = false;
			message += "<bean:message key='error.news.content.length'/>" + '\n';
		}
		
		if (!dateValidate(locale)) {
			val = false;
			<%-- message += "<bean:message key='error.news.date.invalid'/>" + '\n'; --%>
		}
		if (!val)
			alert(message);
		else
			formateDateForServer();
		message="";
		return val;
	} else {
		var date = document.getElementById('date').value;
		var newDate = 2014 + '-' + 01 + '-' + 01;
		document.getElementById('date').value = newDate;
		return true;
	}
}

function dateValidate() {

	var date = document.getElementById('fakeDate').value;
	var rxDatePattern = /^(\d{1,2})(\/)(\d{1,2})(\/)(\d{4})$/;
	var dtArray = date.match(rxDatePattern);
	
	if (date == "") {
        message += "<bean:message key='error.news.date.required'/>" + '\n';
        return false;                
    }
    
	if (dtArray == null ) {
        message += "<bean:message key='error.news.date.invalid'/>" + '\n';
        return false;                
    }
	
	var currVal = getOriginalDate(date);    
    rxDatePattern = /^(\d{4})(-)(\d{1,2})(-)(\d{1,2})$/; 
    dtArray = currVal.match(rxDatePattern); 
    
    if (dtArray == null) {
        message += "<bean:message key='error.news.date.invalid'/>" + '\n';
        return false;                
    }	
    
    dtMonth = dtArray[3];
    dtDay= dtArray[5];
    dtYear = dtArray[1];        
    
    if (dtMonth < 1 || dtMonth > 12) { 
    	message += "<bean:message key='error.news.date.wrongMonth'/>" + '\n';
        return false;
    }
    else if (dtDay < 1 || dtDay> 31) {
   		message += "<bean:message key='error.news.date.wrongDay'/>" + '\n';
        return false;
    }
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) {  
    	message += "<bean:message key='error.news.date.wrongMonth.wrongDay'/>" + '\n';   
        return false;
    }
    else if (dtMonth == 2) 
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap))
        { 
        	message += dtYear + " " + "<bean:message key='error.news.date.year.notLeap'/>" + '\n'; 
        	return false;
        }
    }
    
    return true;
}


function formateDateForUser() {	
	var date;
	var YY = dateInput.substring(0, 4);
	var MM = dateInput.substring(5, 7);
	var DD = dateInput.substring(8, 10);

	if (locale === "ru_ru") {
		date = DD + '/' + MM + '/' + YY;
	} else {
		date = MM + '/' + DD + '/' + YY;
	}

	document.getElementById('date').value = date;
}

function formateDateForServer() {
	var date = document.getElementById('fakeDate').value;
	date = getOriginalDate(date);

	document.getElementById('date').value = date;
}

function getOriginalDate(date) {
	var YY = date.substring(0, 4);
	var MM = date.substring(5, 7);
	var DD = date.substring(8, 10);

	if (locale === "ru_ru") {
		DD = date.substring(0, 2);
		MM = date.substring(3, 5);		
	} else {
		DD = date.substring(3, 5);
		MM = date.substring(0, 2);
	}

	var YY = date.substring(6, 10);

	date = YY + "-" + MM + "-" + DD;
	
	return date; 		
}



	



