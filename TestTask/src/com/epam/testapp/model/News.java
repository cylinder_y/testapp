package com.epam.testapp.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.annotations.Type;

/**
 * The entity class to store news
 * 
 * @author Dzenisiuk Yaraslau
 */
@Entity
@Table(name = "NEWS")
@NamedQuery(name = "News.Delete", query = "DELETE FROM News n WHERE n.id IN (:idList)")
public class News implements Serializable {

	private static final long serialVersionUID = 5314206789595770139L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(generator = "NEWS_SEQ", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "NEWS_SEQ", sequenceName = "NEWS_SEQ", allocationSize = 1)
	private long id;

	@Column(name = "TITLE", nullable = false, length = 100)
	private String title;

	@Column(name = "DATE_", nullable = false)
	@Type(type = "java.sql.Date")
	private Date date;

	@Column(name = "BRIEF", nullable = false, length = 500)
	private String brief;

	@Column(name = "CONTENT", nullable = false, length = 2048)
	private String content;

	public News() {
		date = new Date((new java.util.Date()).getTime());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (getClass() != o.getClass())
			return false;
		News other = (News) o;
		if (title != null ? !title.equals(other.title) : other.title != null)
			return false;
		if (id != 0 ? !(id == other.id) : other.id != 0)
			return false;
		if (!DateUtils.isSameDay(date, other.date))
			return false;
		if (brief != null ? !brief.equals(other.brief) : other.brief != null)
			return false;
		if (content != null ? !content.equals(other.content)
				: other.content != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + (id != 0 ? id : 0));
		result = prime * result + (title != null ? title.hashCode() : 0);
		result = prime * result + (date != null ? date.hashCode() : 0);
		result = prime * result + (brief != null ? brief.hashCode() : 0);
		result = prime * result + (content != null ? content.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		StringBuilder sBuilder = new StringBuilder(((Long) id).toString()
				.length()
				+ title.length()
				+ date.toString().length()
				+ brief.length() + content.length() + 70);
		sBuilder.append("News [id - ");
		sBuilder.append(id);
		sBuilder.append("\ntitle - ");
		sBuilder.append(title);
		sBuilder.append("\ndate - ");
		sBuilder.append(date);
		sBuilder.append("\nbrief - ");
		sBuilder.append(brief);
		sBuilder.append("\ncontent - ");
		sBuilder.append(content);
		sBuilder.append(']');
		return sBuilder.toString();
	}
}
