/*
 * %W% %E% Yaraslau Dzenisiuk
 *
 * Copyright (c)
 */
package com.epam.testapp.database.pool;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

import com.epam.testapp.exception.system.connection.ConnectionException;

/**
 * Connection pool is used to create some set of connections once while
 * application exists. Because create connection is more expensive operation
 * than DML-operations and etc.
 * 
 * @author Yaraslau Dzenisiuk
 */
public final class ConnectionPool implements IConnectionPool, Serializable {

	private static final long serialVersionUID = 8817712154679096659L;

	/**
	 * set of collections
	 */
	private BlockingQueue<Connection> connections;

	private List<Connection> connectionList;

	private String dBUrl;

	private String dBUser;

	private String dBPassword;

	private String driver;

	private int poolSize;

	/**
	 * constant used to lock monitor
	 */
	private static final ReentrantLock LOCK = new ReentrantLock();

	/**
	 * Constructs set of connections. Connections count = max connections count
	 * from config file
	 */
	public ConnectionPool() {

	}

	public String getdBUrl() {
		return dBUrl;
	}

	public void setdBUrl(String dBUrl) {
		this.dBUrl = dBUrl;
	}

	public String getdBUser() {
		return dBUser;
	}

	public void setdBUser(String dBUser) {
		this.dBUser = dBUser;
	}

	public String getdBPassword() {
		return dBPassword;
	}

	public void setdBPassword(String dBPassword) {
		this.dBPassword = dBPassword;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}

	public List<Connection> getConnectionList() {
		return connectionList;
	}

	public void setConnectionList(List<Connection> connectionList) {
		this.connectionList = connectionList;
	}

	public BlockingQueue<Connection> getConnections() {
		return connections;
	}

	public void setConnections(BlockingQueue<Connection> connections) {
		this.connections = connections;
	}

	@Override
	public void init() throws InstantiationException, IllegalAccessException,
			ClassNotFoundException, ConnectionException {
		int poolSize = getPoolSize();
		setConnections(new LinkedBlockingQueue<Connection>(poolSize));
		setConnectionList(new ArrayList<Connection>());
		Class.forName(getDriver()).newInstance();
		initConnections();
	}

	/**
	 * Opens connections
	 */
	private void initConnections() throws ConnectionException {
		for (int i = 0; i < getPoolSize(); i++) {
			addConnection();
		}
	}

	/**
	 * Adds connection to set of connections
	 */
	private Connection addConnection() throws ConnectionException {
		Connection conn = createConnection();
		getConnectionList().add(conn);
		getConnections().offer(conn);

		return conn;
	}

	/**
	 * Creates connection
	 */
	private Connection createConnection() throws ConnectionException {
		try {
			return DriverManager.getConnection(getdBUrl(), getdBUser(),
					getdBPassword());
		} catch (SQLException e) {
			throw new ConnectionException(e);
		}
	}

	@Override
	public Connection openConnection() throws ConnectionException {
		try {
			Connection connection = getConnections().take();
			LOCK.lock();
			try {
				if (connection.isClosed()) {
					int indexOfClosedConnection = getConnectionList().indexOf(
							connection);
					getConnectionList().add(indexOfClosedConnection, null);
					return addConnection();
				}
			} finally {
				LOCK.unlock();
			}

			return connection;
		} catch (InterruptedException | SQLException e) {
			throw new ConnectionException(e.getMessage(), e);
		}
	}

	@Override
	public void closeConnection(Connection connection) {
		getConnections().offer(connection);
	}

	@Override
	public void close() throws ConnectionException {
		int size = getConnectionList().size();
		for (int i = 0; i < size; i++) {
			Connection con = getConnectionList().get(i);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					throw new ConnectionException(e.getMessage(), e);
				}
			}
		}
	}
}
