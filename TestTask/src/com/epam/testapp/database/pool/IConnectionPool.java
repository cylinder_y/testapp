package com.epam.testapp.database.pool;

import java.sql.Connection;

import com.epam.testapp.exception.system.connection.ConnectionException;

/**
 * Interface for connection pool
 * 
 * @param <PK>
 *            type of primary key
 */
public interface IConnectionPool {

	/**
	 * Initialize the connection pool
	 * 
	 * @throws NewsException
	 */
	public abstract void init() throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, ConnectionException;

	/**
	 * Method of closing the connection pool
	 * 
	 * @throws NewsException
	 */
	public abstract void close() throws ConnectionException;

	/**
	 * Method for obtaining the connection
	 * 
	 * @return
	 */
	public abstract Connection openConnection() throws ConnectionException;

	/**
	 * Method to return the obtained connection
	 * 
	 * @param connection
	 *            - the obtained connection
	 */
	public abstract void closeConnection(Connection connection);

}
