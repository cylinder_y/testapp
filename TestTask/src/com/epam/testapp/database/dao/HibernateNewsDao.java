package com.epam.testapp.database.dao;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.epam.testapp.exception.system.dao.DaoException;
import com.epam.testapp.model.News;

/**
 * Implements {@link INewsDao} to realize NewsDao using hibernate
 */
public final class HibernateNewsDao implements INewsDao, Serializable {

	private static final long serialVersionUID = -4230745073245007673L;

	private final String SQL_QUERY_NAME = "News.Delete";

	private final String SQL_DELETE_PARAMETER = "idList";

	/**
	 * @see {@link SessionFactory}
	 */
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<News> getList() throws DaoException {

		List<News> newsList;
		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(News.class);
		criteria.addOrder(Order.desc("date"));
		criteria.addOrder(Order.desc("id"));
		newsList = criteria.list();
		session.getTransaction().commit();

		return newsList;
	}

	@Override
	public News fetchById(long id) throws DaoException {
		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		News news = (News) session.get(News.class, id);
		session.getTransaction().commit();
		return news;
	}

	@Override
	public boolean remove(long[] ids) throws DaoException {

		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		Query query = session.getNamedQuery(SQL_QUERY_NAME);
		query.setParameterList(SQL_DELETE_PARAMETER,
				Arrays.asList(ArrayUtils.toObject(ids)));
		int count = query.executeUpdate();
		session.getTransaction().commit();

		return count == ids.length ? true : false;
	}

	@Override
	public long save(News news) throws DaoException {

		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		session.saveOrUpdate(news);
		session.getTransaction().commit();

		return news.getId();
	}

}
