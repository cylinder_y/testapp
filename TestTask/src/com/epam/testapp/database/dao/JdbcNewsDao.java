package com.epam.testapp.database.dao;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import com.epam.testapp.database.pool.IConnectionPool;
import com.epam.testapp.exception.system.connection.ConnectionException;
import com.epam.testapp.exception.system.dao.DaoException;
import com.epam.testapp.model.News;

/**
 * Extends {@link INewsDao} to realize NewsDao using jdbc
 */
public final class JdbcNewsDao implements INewsDao, Serializable {

	private static final long serialVersionUID = -2602822247344400484L;

	private static final String SQL_ID_FIELD = "ID";

	private static final String SQL_TITLE_FIELD = "TITLE";

	private static final String SQL_DATE_FIELD = "DATE_";

	private static final String SQL_BRIEF_FIELD = "BRIEF";

	private static final String SQL_CONTENT_FIELD = "CONTENT";

	private static final String SQL_SELECT_NEWS_BY_ID = "SELECT * FROM NEWS WHERE ID = ?";

	private static final String SQL_SELECT_NEWS = "SELECT * FROM NEWS ORDER BY NEWS.DATE_ DESC, NEWS.ID DESC";

	private static final String SQL_INSERT_NEWS = "BEGIN INSERT INTO NEWS (TITLE, DATE_, BRIEF, CONTENT) VALUES (?,?,?,?) RETURNING \"ID\" INTO ?; END;";

	private static final String SQL_UPDATE_NEWS = "UPDATE NEWS SET TITLE=?, DATE_=?, BRIEF=?, CONTENT=? WHERE ID=?";

	private static final String SQL_DELETE_NEWS = "DELETE FROM NEWS WHERE ID IN";

	private static final int SQL_ID_PARAM_NUMBER = 5;

	private IConnectionPool connectionPool;

	/**
	 * Default constructor
	 */
	public JdbcNewsDao() {
	}

	public IConnectionPool getConnectionPool() {
		return connectionPool;
	}

	public void setConnectionPool(IConnectionPool connectionPool) {
		this.connectionPool = connectionPool;
	}

	/**
	 * Gets connection to data base
	 */
	private Connection getConnection() throws DaoException {

		try {
			Connection connection = connectionPool.openConnection();

			return connection;

		} catch (ConnectionException e) {
			throw new DaoException(e);
		}
	}

	/**
	 * Close connection
	 * 
	 * @throws DaoException
	 */
	private void closeConnection(Connection connection) {
		connectionPool.closeConnection(connection);
	}

	/**
	 * Parse ResultSet and returns news list relevant ResultSet's content.
	 */
	private List<News> parseResultSet(ResultSet rs) throws DaoException {

		List<News> newsList = new ArrayList<>();
		try {
			while (rs.next()) {
				News news = new News();
				news.setId(rs.getLong(SQL_ID_FIELD));
				news.setTitle(rs.getString(SQL_TITLE_FIELD));
				news.setContent(rs.getString(SQL_CONTENT_FIELD));
				news.setDate(rs.getDate(SQL_DATE_FIELD));
				news.setBrief(rs.getString(SQL_BRIEF_FIELD));
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		}

		return newsList;
	}

	/**
	 * Initialize args of insert query according to news fields values.
	 */
	private void prepareStatementForInsert(CallableStatement statement,
			News news) throws DaoException {

		try {
			prepareStatementWithoutId(statement, news);
			statement.registerOutParameter(SQL_ID_PARAM_NUMBER,
					OracleTypes.NUMBER);
		} catch (SQLException e) {
			throw new DaoException(e);
		}
	}

	/**
	 * Initialize args of update query according to news fields values.
	 */
	private void prepareStatementForUpdate(PreparedStatement statement,
			News news) throws DaoException {
		try {
			prepareStatementWithoutId(statement, news);
			statement.setLong(SQL_ID_PARAM_NUMBER, news.getId());
		} catch (SQLException e) {
			throw new DaoException(e);
		}
	}

	private void prepareStatementWithoutId(PreparedStatement statement,
			News news) throws SQLException {
		int i = 0;
		statement.setString(++i, news.getTitle());
		statement.setDate(++i, news.getDate());
		statement.setString(++i, news.getBrief());
		statement.setString(++i, news.getContent());
	}

	/**
	 * Initialize args of delete query according to news fields values.
	 */
	private void prepareStatementForDelete(PreparedStatement statement,
			long[] ids) throws DaoException {

		try {
			for (int i = 0; i < ids.length; i++) {
				statement.setLong(i + 1, ids[i]);
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public List<News> getList() throws DaoException {

		String sql = SQL_SELECT_NEWS;
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(sql);
				ResultSet rs = statement.executeQuery();) {

			return parseResultSet(rs);

		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			closeConnection(connection);
		}
	}

	@Override
	public News fetchById(long id) throws DaoException {

		List<News> list;
		String query = SQL_SELECT_NEWS_BY_ID;
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(query)) {
			statement.setLong(1, id);

			try (ResultSet rs = statement.executeQuery()) {
				list = parseResultSet(rs);

				if (list == null || list.size() == 0) {
					throw new DaoException(String.format("%s %d %s",
							"Record with PK = ", id, " not found."));
				}
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			closeConnection(connection);
		}

		return list.get(0);
	}

	@Override
	public boolean remove(long[] ids) throws DaoException {

		if (ids == null) {
			throw new DaoException("Nothing to delete");
		}
		String sql = String.format("%s %s", SQL_DELETE_NEWS,
				createParametersForDelete(ids.length));
		Connection connection = getConnection();
		try (PreparedStatement statement = connection.prepareStatement(sql)) {
			prepareStatementForDelete(statement, ids);
			int count = statement.executeUpdate();
			if (count != ids.length) {
				throw new DaoException(String.format("%s %d",
						"On delete modify wrong count of records:", count));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			closeConnection(connection);
		}

		return true;
	}

	@Override
	public long save(News news) throws DaoException {

		if (news.getId() == 0) {
			return insert(news);
		} else if (update(news)) {
			return news.getId();
		}

		throw new DaoException("News entity has wrong id");
	}

	/**
	 * Inserts entity
	 * 
	 * @param news
	 *            to insert
	 * @return {@code id} if entity was inserted else {@code null}
	 */
	private long insert(News news) throws DaoException {

		String sql = SQL_INSERT_NEWS;
		Connection connection = getConnection();
		try (CallableStatement statement = connection.prepareCall(sql)) {
			prepareStatementForInsert(statement, news);
			int count = statement.executeUpdate();
			if (count != 1) {
				throw new DaoException(String.format("%s %d",
						"On insert modify wrong count of records:", count));
			}
			return statement.getLong(SQL_ID_PARAM_NUMBER);

		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Updates entity
	 * 
	 * @param news
	 *            to update
	 * @return {@code true} if entity was updated
	 */
	private boolean update(News news) throws DaoException {

		if (news.getId() == 0) {
			throw new DaoException(String.format("%s %d %s",
					"Record with PK =", news.getId(), "not found."));
		}
		String sql = SQL_UPDATE_NEWS;
		Connection connection = getConnection();

		try (PreparedStatement statement = connection.prepareStatement(sql)) {
			prepareStatementForUpdate(statement, news);
			int count = statement.executeUpdate();
			if (count != 1) {
				throw new DaoException(String.format("%s %d",
						"On update modify wrong count of records:", count));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			closeConnection(connection);
		}

		return true;
	}

	/**
	 * Creates parameters for delete. Example with 5 parameters :<br>
	 * DELETE * FROM * TABLE WHERE ID IN (?,?,?,?,?)
	 * 
	 * @param parametersCount
	 *            count of parameters
	 * */
	public String createParametersForDelete(int parametersCount) {
		StringBuilder builder = new StringBuilder(parametersCount * 2 + 1);
		builder.append("(");
		for (int i = 0; i < parametersCount - 1; i++) {
			builder.append("?,");
		}
		builder.append("?)");
		return builder.toString();
	}

}
