package com.epam.testapp.database.dao;

import java.util.List;

import com.epam.testapp.exception.system.dao.DaoException;
import com.epam.testapp.model.News;

/**
 * Interface for manipulating persist state of entities
 * 
 * @param <PK>
 *            type of primary key
 */
public interface INewsDao {

	/**
	 * Returns all exists entities
	 * 
	 * @return list of entities
	 * 
	 */
	public abstract List<News> getList() throws DaoException;

	/**
	 * Returns entity by field - id
	 * 
	 * @param id
	 *            id of entity
	 * @return entity that was find
	 */
	public abstract News fetchById(long id) throws DaoException;

	/**
	 * Deletes entity by id
	 * 
	 * @param entity
	 *            to delete
	 * @return {@code true} if entity was deleted
	 */
	public abstract boolean remove(long[] ids) throws DaoException;

	/**
	 * Saves entity
	 * 
	 * @param entity
	 *            entity to save
	 * @return id of saved entity
	 */
	public abstract long save(News news) throws DaoException;
}