package com.epam.testapp.database.dao;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.ArrayUtils;

import com.epam.testapp.exception.system.dao.DaoException;
import com.epam.testapp.model.News;

/**
 * Implements {@link INewsDao} to realize NewsDao using JPA
 */
public final class JpaNewsDao implements INewsDao, Serializable {

	private static final String SQL_QUERY_NAME = "News.Delete";

	private static final String SQL_DELETE_PARAMETER = "idList";

	/**
	 * @see {@link EntityManagerFactory}
	 * 
	 */
	private EntityManagerFactory managerFactory;

	/**
	 * @see {@link EntityManager}
	 * 
	 */
	private EntityManager entityManager;

	public JpaNewsDao() {
	}

	public EntityManagerFactory getManagerFactory() {
		return managerFactory;
	}

	public void setManagerFactory(EntityManagerFactory managerFactory) {
		this.managerFactory = managerFactory;
	}

	/**
	 * Initialize entityManger to interact with persist context
	 * 
	 * */
	public void init() {
		entityManager = managerFactory.createEntityManager();
	}

	@Override
	public List<News> getList() throws DaoException {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<News> cq = cb.createQuery(News.class);
		Root<News> rootEntry = cq.from(News.class);
		CriteriaQuery<News> all = cq.select(rootEntry);
		all.orderBy(cb.desc(rootEntry.get("date")),
				cb.desc(rootEntry.get("id")));
		TypedQuery<News> allQuery = entityManager.createQuery(all);
		return allQuery.getResultList();
	}

	@Override
	public News fetchById(long id) throws DaoException {
		return entityManager.find(News.class, id);
	}

	@Override
	public boolean remove(long[] ids) throws DaoException {
		entityManager.getTransaction().begin();
		Query query = entityManager.createNamedQuery(SQL_QUERY_NAME);
		query.setParameter(SQL_DELETE_PARAMETER,
				Arrays.asList(ArrayUtils.toObject(ids)));
		int count = query.executeUpdate();
		entityManager.getTransaction().commit();

		return count == ids.length ? true : false;
	}

	@Override
	public long save(News news) throws DaoException {
		entityManager.getTransaction().begin();
		News savedNews = entityManager.merge(news);
		entityManager.getTransaction().commit();
		return savedNews.getId();
	}
}
