package com.epam.testapp.presentation.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

import com.epam.testapp.model.News;

/**
 * Implements{@link ActionForm} to describe news form
 * */
public class NewsForm extends ActionForm {

	private static final long serialVersionUID = 5345464389107932933L;

	/**
	 * News will be saved/edited/deleted
	 * */
	private News news;

	/**
	 * News list
	 * 
	 * */
	private List<News> newsList;

	/**
	 * Id's of selected news to delete
	 * 
	 * */
	private long[] ids;

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public List<News> getNewsList() {
		return newsList;
	}

	public void setNewsList(List<News> newsList) {
		this.newsList = newsList;
	}

	public long[] getIds() {
		return ids;
	}

	public void setIds(long[] multiBox) {
		this.ids = multiBox;
	}

	public NewsForm() {
		news = new News();
	}

}