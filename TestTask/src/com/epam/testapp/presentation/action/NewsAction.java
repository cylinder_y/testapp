package com.epam.testapp.presentation.action;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.epam.testapp.database.dao.INewsDao;
import com.epam.testapp.exception.system.SystemException;
import com.epam.testapp.model.News;
import com.epam.testapp.presentation.form.NewsForm;

/**
 * An action class for news management
 * 
 * @author Dzenisiuk Yaraslau
 * 
 */
public class NewsAction extends DispatchAction {

	private static final String ADD_NEWS_FORWARD = "addNews";

	private static final String LIST_VIEW_FORWARD = "listView";

	private static final String NEWS_VIEW_FORWARD = "newsView";

	private static final String EDIT_NEWS_FORWARD = "editNews";

	private static final String LOCALE_PARAMETER = "locale";

	private static final String PREVIOUS_PAGE_ATTRIBUTE = "previosPage";

	private static final String REFERER_HEADER = "referer";

	private static final String LIST_VIEW_REDIRECT = "Action.do?method=listView";

	private static final String NEWS_VIEW_REDIRECT = "Action.do?method=newsView";

	private INewsDao newsDao;

	public INewsDao getNewsDao() {
		return newsDao;
	}

	public void setNewsDao(INewsDao newsDao) {
		this.newsDao = newsDao;
	}

	public NewsAction() {
	}

	/**
	 * Forwards to add news page
	 */
	public ActionForward addNews(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		NewsForm newsForm = (NewsForm) form;
		newsForm.setNews(new News());

		return mapping.findForward(ADD_NEWS_FORWARD);
	}

	/**
	 * Fill form with list of news<br>
	 * Forwards to the list page
	 * 
	 * @throws SystemException
	 */
	public ActionForward listView(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {

		List<News> newsList = newsDao.getList();
		NewsForm newsForm = (NewsForm) form;
		newsForm.setNewsList(newsList);
		newsForm.setIds(new long[0]);

		return mapping.findForward(LIST_VIEW_FORWARD);
	}

	/**
	 * Fill form with single news<br>
	 * Forwards to the view page of single news
	 * 
	 * @throws SystemException
	 */
	public ActionForward newsView(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {

		NewsForm newsForm = (NewsForm) form;
		long newsId = newsForm.getIds()[0];
		News news = newsDao.fetchById(newsId);
		newsForm.setNews(news);

		return mapping.findForward(NEWS_VIEW_FORWARD);
	}

	/**
	 * Sets selected language. Forwards to current page
	 * 
	 * @throws IOException
	 */
	public ActionForward switchLanguage(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		Locale locale = new Locale(request.getParameter(LOCALE_PARAMETER));
		request.getSession().setAttribute(Globals.LOCALE_KEY, locale);

		response.sendRedirect(request.getHeader(REFERER_HEADER));

		return mapping.findForward(LIST_VIEW_FORWARD);
	}

	/**
	 * Save news. Forwards to view page
	 * 
	 * @throws SystemException
	 * @throws IOException
	 */
	public void save(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException, IOException {

		NewsForm newsForm = (NewsForm) form;
		News news = newsForm.getNews();
		long[] ids = { newsDao.save(news) };
		newsForm.setIds(ids);

		response.sendRedirect(NEWS_VIEW_REDIRECT);
	}

	/**
	 * Fill form with single news<br>
	 * Forwards to the edit page of single news
	 * 
	 * @throws SystemException
	 */
	public ActionForward edit(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {

		NewsForm newsForm = (NewsForm) form;
		long newsId = newsForm.getIds()[0];
		News news = newsDao.fetchById(newsId);
		newsForm.setNews(news);

		return mapping.findForward(EDIT_NEWS_FORWARD);
	}

	/**
	 * Delete selected news
	 * 
	 * @throws SystemException
	 * @throws IOException
	 */
	public void delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException, IOException {

		NewsForm newsForm = (NewsForm) form;
		long[] idsToRemove = newsForm.getIds();
		newsDao.remove(idsToRemove);

		response.sendRedirect(LIST_VIEW_REDIRECT);
	}

	/**
	 * Redirect to previous page
	 * 
	 * @throws IOException
	 * 
	 */
	public ActionForward cancel(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		String url = (String) request.getSession().getAttribute(
				PREVIOUS_PAGE_ATTRIBUTE);
		if (url != null) {
			response.sendRedirect(url);
		}

		return mapping.findForward(LIST_VIEW_FORWARD);
	}

}
