package com.epam.testapp.exception.system.connection;

import com.epam.testapp.exception.system.SystemException;

/**
 * Errors of connections
 * 
 * @author Dzenisiuk Yaraslau
 * 
 */
public class ConnectionException extends SystemException {
	public ConnectionException() {
	}

	public ConnectionException(String message) {
		super(message);
	}

	public ConnectionException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConnectionException(Throwable cause) {
		super(cause);
	}

	public ConnectionException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
