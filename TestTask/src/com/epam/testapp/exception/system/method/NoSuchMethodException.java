package com.epam.testapp.exception.system.method;

import com.epam.testapp.exception.system.SystemException;

/**
 * Errors occurs when used nonexistent actions method
 * 
 * @author Dzenisiuk Yaraslau
 * 
 */
public class NoSuchMethodException extends SystemException {
	public NoSuchMethodException() {
	}

	public NoSuchMethodException(String message) {
		super(message);
	}

	public NoSuchMethodException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoSuchMethodException(Throwable cause) {
		super(cause);
	}

	public NoSuchMethodException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
