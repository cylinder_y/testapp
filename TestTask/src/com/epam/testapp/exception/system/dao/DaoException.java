package com.epam.testapp.exception.system.dao;

import com.epam.testapp.exception.system.SystemException;

/**
 * Errors of dao layer
 * 
 * @author Dzenisiuk Yaraslau
 * 
 */
public class DaoException extends SystemException {
	public DaoException() {
	}

	public DaoException(String message) {
		super(message);
	}

	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}

	public DaoException(Throwable cause) {
		super(cause);
	}

	public DaoException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
