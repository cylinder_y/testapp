package com.epam.testapp.exception.handler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ExceptionHandler;
import org.apache.struts.config.ExceptionConfig;

/**
 * ExceptionHandler implementation class GlobalExceptionHandler. Used to log
 * errors
 */
public final class GlobalExceptionHandler extends ExceptionHandler {

	private static final Logger logger = Logger
			.getLogger(GlobalExceptionHandler.class);

	@Override
	public ActionForward execute(Exception ex, ExceptionConfig ae,
			ActionMapping mapping, ActionForm formInstance,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException {

		logger.fatal(getStackTrace(ex));

		return super.execute(ex, ae, mapping, formInstance, request, response);
	}

	/**
	 * 
	 * @param ex
	 *            to get stack trace
	 * @return stack trace of {@code ex}
	 */
	private String getStackTrace(Exception ex) {
		String logInfo = "Cause : ";
		StringBuilder sBuilder = new StringBuilder(logInfo.length()
				+ ex.toString().length() + getStackTraceLength(ex));

		sBuilder.append(logInfo);
		sBuilder.append(ex);
		for (StackTraceElement element : ex.getStackTrace()) {
			sBuilder.append(element);
			sBuilder.append(System.lineSeparator());
		}

		return sBuilder.toString();
	}

	/**
	 * 
	 * @param ex
	 *            to get stack trace
	 * @return length of stack trace string
	 */
	private int getStackTraceLength(Exception ex) {
		int length = 0;

		for (StackTraceElement element : ex.getStackTrace()) {
			length += element.toString().length()
					+ System.lineSeparator().length();
		}
		return length;
	}

}
